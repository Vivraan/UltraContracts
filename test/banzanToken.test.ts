import { expect } from "chai";
import { ethers, upgrades } from "hardhat";
import { BanzanToken } from "../typechain";

const NAME_BANZAN_TOKEN = "BanzanToken";

describe(NAME_BANZAN_TOKEN, () => {
    it("Should have symbol ZAN (for now)", async () => {
        const BanzanToken = await ethers.getContractFactory(NAME_BANZAN_TOKEN);
        const banzanToken = await upgrades.deployProxy(BanzanToken, { kind: 'uups' }) as BanzanToken;

        expect(await banzanToken.symbol()).to.equal("ZAN");
    });

    it(`Should not be able to attach to existing contract deployments for ${NAME_BANZAN_TOKEN}`, async () => {
        const BanzanToken = await ethers.getContractFactory(NAME_BANZAN_TOKEN);
        // Some contract address taken from a previous deployment
        expect(BanzanToken.attach("0xCf7Ed3AccA5a467e9e704C703E8D87F634fB0Fc9")).to.throw;
    });
})
