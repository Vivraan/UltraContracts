import { expect } from "chai";
import { BigNumber } from "ethers";
import { ethers, upgrades } from "hardhat";
import { BanzanToken } from "../typechain";


describe("Claim Reward functionality", () => {
  it("Disburser should disburse rewards from allowance", async () => {
    const NAME_BANZAN_TOKEN = "BanzanToken";
    /** 10^18 */
    /** same as 2^256 - 1 */
    const APPROVE_AMOUNT = BigNumber.from(-1).toTwos(256);

    // Deploy BanzanToken
    const BanzanToken = await ethers.getContractFactory(NAME_BANZAN_TOKEN);
    const banzanTokenAsDeployer = await upgrades.deployProxy(BanzanToken, { kind: "uups" }) as BanzanToken;
    await banzanTokenAsDeployer.deployed();
    console.log(`Deployed ${NAME_BANZAN_TOKEN} contract at ${banzanTokenAsDeployer.address}`);

    const decimals = await banzanTokenAsDeployer.decimals();
    console.log(`Set decimals = ${decimals}`);
    const decimalFactor = BigNumber.from(10).pow(decimals);
    const initMintAmount = BigNumber.from(10_000_000_000).mul(decimalFactor);

    const [deployer, disburser] = await ethers.getSigners();
    const banzanTokenAsDisburser = banzanTokenAsDeployer.connect(disburser);

    // Mint amount to deployer
    console.log(`Minting ${initMintAmount} to deployer ${deployer.address}`);
    expect(banzanTokenAsDeployer.mint(deployer.address, initMintAmount)).to.not.be.reverted;

    // Approve externally
    console.log(`Increasing allowance of disburser ${disburser.address} to ${APPROVE_AMOUNT}`);
    expect(banzanTokenAsDeployer.increaseAllowance(disburser.address, APPROVE_AMOUNT))
      .to.not.be.reverted;

    // Send some reward over to the disburser (can be anyone)
    const reward = BigNumber.from(decimalFactor).div(100_000);
    console.log(`Trying to Disburse ${reward} from ${deployer.address} to ${disburser.address}`);

    await expect(banzanTokenAsDisburser.transferFrom(deployer.address, disburser.address, reward))
      .to.not.be.reverted;

    console.log(`Checking if ${disburser.address} received ${reward} ZAN`);
    expect(await banzanTokenAsDeployer.balanceOf(disburser.address)).to.equal(reward);

    const balance = initMintAmount.sub(reward);
    console.log(`Checking if ${deployer.address} now has ${balance} ZAN`);
    expect(await banzanTokenAsDeployer.balanceOf(deployer.address)).to.equal(balance);
  });
});
