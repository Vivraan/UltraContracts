// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
import { BigNumber } from "ethers";
import { ethers, upgrades } from "hardhat";
import { BanzanToken } from "../typechain";

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  // We get the contract(s) to deploy
  const NAME_BANZAN_TOKEN = "BanzanToken";

  /// DEPLOYED ONCE AT 0x50d5858676cD2cDd236923730025ba65ec3c4f9F

  // Who's deploying?
  const [deployer, disburser] = await ethers.getSigners();
  console.log("Deploying contracts with the account: ", deployer.address);
  console.log("Account balance: ", (await deployer.getBalance()).toString());

  // Deploy BanzanToken
  const BanzanToken = await ethers.getContractFactory(NAME_BANZAN_TOKEN);
  const banzanToken = await upgrades.deployProxy(BanzanToken, { kind: "uups" }) as BanzanToken;
  await banzanToken.deployed();
  console.log(`Deployed ${NAME_BANZAN_TOKEN} contract at ${banzanToken.address}`);

  const decimals = await banzanToken.decimals();
  console.log(`Set decimals = ${decimals}`);
  const decimalFactor = BigNumber.from(10).pow(decimals);
  const initMintAmount = BigNumber.from(10_000_000_000).mul(decimalFactor);
  const approveAmount = BigNumber.from(10_000_000).mul(decimalFactor);

  // Mint amount to deployer
  console.log(`Minting ${initMintAmount} to ${deployer.address}`);
  await banzanToken.mint(deployer.address, initMintAmount);

  // Approve externally
  console.log(`Increasing allowance of disburser ${disburser.address} contract to ${approveAmount}`);
  await banzanToken.increaseAllowance(disburser.address, approveAmount);

  // Set up complete.
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
