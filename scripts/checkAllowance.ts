// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
import { BigNumber } from "ethers";
import { ethers, upgrades } from "hardhat";
import { BanzanToken } from "../typechain";

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  // We get the contract(s) to deploy
  const NAME_BANZAN_TOKEN = "BanzanToken";

  /// DEPLOYED ONCE AT 0x50d5858676cD2cDd236923730025ba65ec3c4f9F

  // Who's deploying?
  const [deployer, disburser] = await ethers.getSigners();
  console.log("Deploying contracts with the account: ", deployer.address);
  console.log("Account balance: ", (await deployer.getBalance()).toString());

  // Deploy BanzanToken
  const BanzanToken = await ethers.getContractFactory(NAME_BANZAN_TOKEN);
  const banzanToken = BanzanToken.attach("0x50d5858676cD2cDd236923730025ba65ec3c4f9F");
  console.log(`Deployed ${NAME_BANZAN_TOKEN} contract exists at ${banzanToken.address}`);

  console.log(`Allowance of ${disburser.address} for ${deployer.address}: ${await banzanToken.allowance(deployer.address, disburser.address)}`)
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
